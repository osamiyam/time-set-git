
import http.client
import re
import os
import platform

tr = {
    'Jan': '01', 'Feb': '02', 'Mar': '03', 'Apr': '04',
    'May': '05', 'Jun': '06', 'Jul': '07', 'Aug': '08',
    'Sep': '09', 'Oct': '10', 'Nov': '11', 'Dec': '12',
    }

def settime_unix(dstring):
    mon = dstring[4:7]
    day = int(dstring[8:10])
    hour = int(dstring[11:13])
    min = int(dstring[14:16])
    sec = int(dstring[17:19])
    cur_time = tr[mon] + ("%02d%02d%02d.%02d" % (day, hour, min, sec))
    print("time = " + cur_time)
    os.system("date " + cur_time)
    
def settime_dos(dstring):
    tt = dstring[11:19]
    # os.system("time /t")
    com = "time %s" % tt
    os.system(com)
    mon = dstring[4:7]
    day = int(dstring[8:10])
    year = dstring[22:24]
    comd = "date %s-%s-%02d" % (year, tr[mon], day)
    os.system(comd)

def main():
    server = "ntp-a1.nict.go.jp"
    url = "/cgi-bin/time"
    conn = http.client.HTTPSConnection(server)
    conn.request("GET", url)
    r1 = conn.getresponse()
    dstring = r1.read()
    print(dstring)
    os_type = platform.system()
    print("OS = %s" % os_type)
    if os_type == 'Windows':
        settime_dos(dstring)
    elif os_type == "Darwin":
        settime_unix(dstring)
    else:
        print("Unknown Operating System!")

if __name__ == '__main__':
    main()
    


