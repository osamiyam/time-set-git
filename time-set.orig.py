
import httplib
import re
import os

tr = {
    'Jan': '01', 'Feb': '02', 'Mar': '03', 'Apr': '04',
    'May': '05', 'Jun': '06', 'Jul': '07', 'Aug': '08',
    'Sep': '09', 'Oct': '10', 'Nov': '11', 'Dec': '12',
    }

def main():
    server = "ntp-a1.nict.go.jp"
    url = "/cgi-bin/time"
    conn = httplib.HTTPSConnection(server)
    conn.request("GET", url)
    r1 = conn.getresponse()
    dstring = r1.read()
    mon = dstring[4:7]
    day = int(dstring[8:10])
    hour = int(dstring[11:13])
    min = int(dstring[14:16])
    sec = int(dstring[17:19])
    cur_time = tr[mon] + ("%02d%02d%02d.%02d" % (day, hour, min, sec))
    print "time = " + cur_time
    os.system("date " + cur_time)

if __name__ == '__main__':
    main()
    


